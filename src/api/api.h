#ifndef API_H
#define API_H

#include <lua5.3/lua.h>
#include <lua5.3/lualib.h>
#include <lua5.3/lauxlib.h>

namespace Api
{
  extern void loadApi(lua_State* state);
  extern int changeLang(lua_State* state);
}
#endif //API_H
