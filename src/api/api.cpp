#include "api/api.h"
#include "utils/locale.h"
#include <string>
using namespace std;

namespace Api
{
  void loadApi(lua_State* state)
  {
    lua_register(state, "changeLang", changeLang);
  }

  int changeLang(lua_State* state)
  {
    int args = lua_gettop(state);
    Locale::setLang(lua_tostring(state, 1));
    return 0;
  }
}
