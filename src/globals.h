#ifndef GLOBALS_H
#define GLOBALS_H

#include "utils/parser.h"

class Globals
{
  public:
    static string getConfig(string field);
    static void setConfig(string field, string value);
    virtual ~Globals();

  private:
    static Globals *instance;
    static void checkInstance();

    Globals();

    Parser *parser;
};

#endif //GLOBALS_H
