#ifndef MAINMENU_H
#define MAINMENU_H

#include "utils/parser.h"
#include <RosquilleraReforged/rf_process.h>

class MainMenu_Cursor : public RF_Process
{
  public:
    MainMenu_Cursor():RF_Process("MainMenu_Cursor"){}
    virtual ~MainMenu_Cursor(){}

    void SetGfx(string package, string gfx);
    void SetPosition(Vector2<float> pos);
    Vector2<float> GetPosition();
};

class MainMenu : public RF_Process
{
  public:
    MainMenu():RF_Process("MainMenu"){}
    virtual ~MainMenu();

    virtual void Start();
    virtual void Update();

    bool loadMenu(int target);
    void optionSelected();

  private:
    Parser *parser;
    MainMenu_Cursor *cursor;
    int opc = 0, depth = -1;

    string package = "";
    string font = "";
    string background = "";
    string music = "";

    vector<string> options;
    vector<string> values;

    bool keyPressed = false;
};

#endif //MAINMENU_H
