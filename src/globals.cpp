#include "globals.h"
#include "utils/json.h"

#include <iostream>
using namespace std;

Globals* Globals::instance = nullptr;
void Globals::checkInstance()
{
  if(instance == nullptr)
  {
    new Globals();
  }
}

Globals::Globals()
{
  instance = this;
  parser = new Parser("options.json");
}

Globals::~Globals()
{
  instance = nullptr;
}

string Globals::getConfig(string field)
{
  checkInstance();

  string val;
  Parser::json_string(instance->parser->scenejs["config"][field], val);
  return val;
}

void Globals::setConfig(string field, string value)
{
  checkInstance();

  instance->parser->scenejs["config"][field] = value;
  ofstream salida("options.json");
  Json::StyledStreamWriter writer;
  writer.write(salida, instance->parser->scenejs);
}
