#include "base/actor.h"
#include "api/api.h"
#include <RosquilleraReforged/rf_engine.h>

Actor::~Actor()
{
  if(state != nullptr)
  {
    lua_close(state);
  }
}
void Actor::setLogicScript(string scriptpath)
{
  if(state != nullptr)
  {
    lua_close(state);
  }

  script = scriptpath;
  state = luaL_newstate();

  Api::loadApi(state);

  luaL_openlibs(state);
  int result = luaL_loadfile(state, scriptpath.c_str());
  if(result != LUA_OK)
  {
    logicError();
    return;
  }

  result = lua_pcall(state, 0, LUA_MULTRET, 0);
  if(result != LUA_OK)
  {
    logicError();
    return;
  }

}

void Actor::logicError()
{
  if(state != nullptr)
  {
    // The error message is on top of the stack.
    // Fetch it, print it and then pop it off the stack.
    const char* message = lua_tostring(state, -1);
    RF_Engine::Debug(message);
    lua_pop(state, 1);
  }
}
