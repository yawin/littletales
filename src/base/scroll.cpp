#include "base/scroll.h"
#include <limits>
using namespace std;

Scroll* Scroll::instance = nullptr;

void Scroll::checkInstance()
{
  if(instance == nullptr)
  {
    instance = RF_Engine::getTask<Scroll>(RF_Engine::newTask<Scroll>());
    instance->wind = RF_Engine::getWindow(instance->window);
  }
}

Scroll::~Scroll()
{
  instance = nullptr;
}

Vector2<float> Scroll::getPos()
{
  checkInstance();

  return instance->transform.position - instance->trueOrigin;
}

void Scroll::move(Vector2<float> dir)
{
  move(dir.x, dir.y);
}
void Scroll::move(float x, float y)
{
  checkInstance();
  Vector2<float> destiny = instance->transform.position - instance->trueOrigin;
  destiny.x += x;
  destiny.y += y;
  setPos(destiny);
}

void Scroll::setPos(Vector2<float> pos)
{
  setPos(pos.x, pos.y);
}
void Scroll::setPos(float x, float y)
{
  checkInstance();
  Vector2<float> destiny = Vector2<float>(x,y);

  if(x > 0){ destiny.x = 0; }
  else if(x < -(instance->graph->w - instance->wind->width())){ destiny.x = -(instance->graph->w - instance->wind->width()) - 1;}
  if(y > 0){ destiny.y = 0; }
  else if(y < -(instance->graph->h - instance->wind->height())){ destiny.y = -(instance->graph->h - instance->wind->height()) - 1;}

  instance->transform.position.x = destiny.x + instance->trueOrigin.x;
  instance->transform.position.y = destiny.y + instance->trueOrigin.y;
}

void Scroll::setGraph(SDL_Surface *gfx)
{
  checkInstance();
  instance->zLayer = numeric_limits<int>::max();

  if(gfx != nullptr)
  {
    instance->graph = gfx;
    instance->trueOrigin.x = instance->graph->w*0.5;
    instance->trueOrigin.y = instance->graph->h*0.5;
    instance->transform.position = instance->trueOrigin;
  }
}

void Scroll::clear()
{
  if(instance != nullptr)
  {
    instance->graph = nullptr;
    instance->trueOrigin.x = instance->trueOrigin.y = instance->transform.position.x = instance->transform.position.y = 0;
  }
}

void Scroll::setCam(string id)
{
  checkInstance();
  instance->camID = id;
  instance->camera = (id != "") ? RF_Engine::getTask<Entity>(id) : nullptr;

}

string Scroll::getCam()
{
  checkInstance();
  return instance->camID;
}

const int Scroll::width()
{
  checkInstance();
  if(instance->graph != nullptr)
  {
    return instance->graph->w;
  }
}

const int Scroll::height()
{
  checkInstance();
  if(instance->graph != nullptr)
  {
    return instance->graph->h;
  }
}

const int Scroll::depth()
{
  checkInstance();
  return instance->zLayer;
}

void Scroll::Draw()
{
  if(camera != nullptr)
  {
    Vector2<float> destiny;
    destiny.x = -camera->realPosition.x + wind->width()*0.5;
    destiny.y = -camera->realPosition.y + wind->height()*0.5;

    setPos(destiny);
  }
}
