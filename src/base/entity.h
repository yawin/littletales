#ifndef ENTITY_H
#define ENTITY_H

#include <RosquilleraReforged/rf_process.h>

class Entity : public RF_Process
{
  public:
    Entity(string id = "Entity"):RF_Process(id){}
    virtual ~Entity(){}

    virtual void Draw();
    Vector2<float> realPosition;
};
#endif //ENTITY_H
