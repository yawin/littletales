#include "base/gameobject.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_engine.h>

void GameObject::addAnimation(string animationID, const char* b, int f, float s, bool l)
{
  addAnimation(animationID, "", b, f, s, l);
}

void GameObject::addAnimation(string animationID, const char* p, const char* b, int f, float s, bool l)
{
  animations[animationID] = new Animation(p,b,f,s,l);
  if(animations.size() == 1)
  {
    setActiveAnimation(animationID);
  }
}

bool GameObject::setActiveAnimation(string animationID)
{
  if(animations[animationID] != nullptr)
   {
     if(package == animations[animationID]->package && name == animations[animationID]->basename)
     {
       return true;
     }

     if(RF_AssetManager::isLoaded(animations[animationID]->package, animations[animationID]->basename + "_0"))
     {
        package = animations[animationID]->package;
        name = animations[animationID]->basename;
        framesAmount = animations[animationID]->frameAmount;
        speed = animations[animationID]->speed;
        repeat = animations[animationID]->looped;
        actualFrame = 0.0;
        return true;
      }
      else
      {
        return false;
      }
   }
   else
   {
     return false;
   }
}

SDL_Surface* GameObject::getFrame()
{
  SDL_Surface* ret = nullptr;

  actualFrame+=(speed*RF_Engine::instance->Clock.deltaTime);
  if(actualFrame < 0.0){actualFrame = 0.9+((float)framesAmount);}
  int aF = (int)actualFrame;

  if(aF > framesAmount)
  {
    actualFrame = 0.0;
    if(!repeat)
    {
      name = "";
      return nullptr;
    }
    else
    {
      actualFrame+=(speed*RF_Engine::instance->Clock.deltaTime);
      aF = (int)actualFrame;
    }
  }

  ret = RF_AssetManager::Get<RF_Gfx2D>(package, name+"_"+to_string(aF));
  return ret;
}

void GameObject::Draw()
{
  Entity::Draw();

  if(package != "" && name != "")
  {
    SDL_Surface* gfx = getFrame();
    if(gfx != nullptr)
    {
      graph = gfx;
    }
  }
}
