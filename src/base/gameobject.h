#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "base/entity.h"

#include <map>
using namespace std;

struct Animation
{
  string package, basename;
  int frameAmount;
  float speed;
  bool looped;

  void save(const char* p, const char* b, int f, float s, bool l)
  {
      package = string(p);
      basename = string(b);
      frameAmount = f;
      speed = s;
      looped = l;
  }

  Animation(const char* p, const char* b, int f, float s = 1.0, bool l=true)
  {
    save(p, b, f, s, l);
  }
  Animation(const char* b, int f, float s = 1.0, bool l=true)
  {
    save("", b, f, s, l);
  }
};

class GameObject : public Entity
{
  public:
    GameObject(string id = "GameObject"):Entity(id){}
    virtual ~GameObject(){}

    void addAnimation(string animationID, const char* b, int f, float s = 1.0, bool l=true);
    void addAnimation(string animationID, const char* p, const char* b, int f, float s = 1.0, bool l=true);
    bool setActiveAnimation(string animationID);

    virtual void Draw();

  private:
    bool repeat = true;
    string name = "", package = "";
    int framesAmount = 0;
    float actualFrame = 0.0;
    float speed = 1.0;
    map<string, Animation*> animations;

    SDL_Surface* getFrame();
};

#endif //GAMEOBJECT_H
