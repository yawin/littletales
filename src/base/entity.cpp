#include "base/entity.h"
#include "base/scroll.h"
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_engine.h>

void Entity::Draw()
{
  transform.position = Scroll::getPos() + realPosition;
  zLayer = Scroll::depth() - realPosition.y - 1;
}
