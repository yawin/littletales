#ifndef SCROLL_H
#define SCROLL_H

#include "base/entity.h"

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>

class Scroll : public RF_Process
{
  public:
    Scroll():RF_Process("BackgroundScroll"){}
    virtual ~Scroll();

    static Vector2<float> getPos();
    static void setPos(Vector2<float> pos);
    static void setPos(float x, float y);

    static void move(Vector2<float> dir);
    static void move(float x, float y);
    static void setGraph(SDL_Surface *gfx);
    static void clear();

    static void setCam(string id);
    static string getCam();

    static const int width();
    static const int height();
    static const int depth();

    virtual void Draw();

  private:
    static Scroll* instance;

    static void checkInstance();
    Vector2<float> trueOrigin;
    RF_Window* wind;

    string camID = "";
    Entity* camera = nullptr;
};

#endif //SCROLL_H
