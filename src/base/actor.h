#ifndef ACTOR_H
#define ACTOR_H

#include "base/gameobject.h"
#include <lua5.3/lua.h>
#include <lua5.3/lualib.h>
#include <lua5.3/lauxlib.h>

class Actor : public GameObject
{
  public:
    Actor(string id = "Actor") : GameObject(id){}
    virtual ~Actor();

    void setLogicScript(string scriptpath);

  private:
    string script;
    lua_State *state = nullptr;

    void logicError();
};

#endif //ACTOR_H
