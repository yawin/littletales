#ifndef GAME_H
#define GAME_H

#include "base/scroll.h"
#include "base/actor.h"
#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>

class Game : public RF_Process
{
  public:
    Game():RF_Process("Gametest"){}
    virtual ~Game(){ Scroll::clear();}

    virtual void Start()
    {
      RF_AssetManager::LoadAssetPackage("resources/gametest");
      Scroll::setGraph(RF_AssetManager::Get<RF_Gfx2D>("gametest", "Pueblo"));

      Actor* actor = RF_Engine::getTask<Actor>(RF_Engine::newTask<Actor>(id));
      actor->addAnimation("idle", "gametest", "fuente", 3, 4.0, true);
      actor->realPosition=Vector2<float>(300,500);
      actor->setLogicScript("scripts/test.lua");
    }

    virtual void Update()
    {
    }
};

#endif //GAME_H
