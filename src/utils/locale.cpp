#include "locale.h"
#include "utils/parser.h"

namespace Locale
{
  static string lang = "";

  void setLang(string language)
  {
    lang = language;
  }

  string getLang()
  {
    return lang;
  }

  string getText(string key)
  {
    if(lang == "")
    {
      return "Language not configured";
    }

    Parser parser("locale/" + lang + ".json");
    string tmp = "";
    Parser::json_string(parser.scenejs[key], tmp);
    return tmp;
  }
}
