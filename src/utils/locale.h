#ifndef LOCALE_H
#define LOCALE_H

#include <string>
using namespace std;

namespace Locale
{
  extern void setLang(string language);

  extern string getLang();

  extern string getText(string key);
}

#endif //LOCALE_H
