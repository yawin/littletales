#include "mainmenu.h"
#include "mainprocess.h"
#include "gui.h"
#include "utils/locale.h"
#include "globals.h"

#include <RosquilleraReforged/rf_soundmanager.h>
#include <RosquilleraReforged/rf_taskmanager.h>

MainMenu::~MainMenu()
{
  delete parser;
  parser = nullptr;
  cursor = nullptr;

  for(string t : options)
  {
    RF_TextManager::DeleteText(t);
  }

  stopGUI();
}

void MainMenu_StartButton(int foo)
{
  MainProcess::instance->ChangeScene(_GAME);
}

void MainMenu_Cursor::SetGfx(string package, string gfx)
{
  graph = RF_AssetManager::Get<RF_Gfx2D>(package, gfx);
}
void MainMenu_Cursor::SetPosition(Vector2<float> pos)
{
  transform.position = pos;
}

Vector2<float> MainMenu_Cursor::GetPosition()
{
  return transform.position;
}

bool MainMenu::loadMenu(int target)
{
    bool errores = false;

    if(depth != target)
    {
      depth = target;
      for(string t : options)
      {
        RF_TextManager::DeleteText(t);
      }
      options.clear();
      values.clear();

      Vector2<float> pos;
      SDL_Color col;
      string key, value;

      if(depth > 0)
      {
        errores |= !Parser::json_vector2float(parser->scenejs["back_text"]["position"], pos);
        errores |= !Parser::json_SDLColor(parser->scenejs["back_text"]["color"], col);
        key = "mainmenu_back";
        value = "menu_back";

        options.push_back(RF_TextManager::Write<float>(Locale::getText(key), col, pos));
        values.push_back(value);
      }

      for(Json::Value jv : parser->scenejs["menu_levels"][to_string(depth)])
      {
        errores |= !Parser::json_vector2float(jv["position"], pos);
        errores |= !Parser::json_SDLColor(jv["color"], col);
        errores |= !Parser::json_string(jv["key"], key);
        errores |= !Parser::json_string(jv["value"], value);

        options.push_back(RF_TextManager::Write<float>(Locale::getText(key), col, pos));
        values.push_back(value);
      }

      opc = 0;

      if(errores)
      {
        RF_Engine::Debug("Error al parsear json");
      }
    }

    return errores;
}

void MainMenu::Start()
{
  transform.position.x = RF_Engine::getWindow(window)->width()*0.5;
  transform.position.y = RF_Engine::getWindow(window)->height()*0.5;
  parser = new Parser("scenes/mainmenu.json");

  zLayer = 1001;

  bool errores = false;
  errores |= !Parser::json_string(parser->scenejs["package"], package);
  errores |= !Parser::json_string(parser->scenejs["font"], font);
  errores |= !Parser::json_string(parser->scenejs["background"], background);
  errores |= !Parser::json_string(parser->scenejs["music"], music);

  initGUI(package, font, id);
  RF_Engine::getTask<Mouse>(gui_mouse)->zLayer = 9999;

  if(background != "")
  {
    graph = RF_AssetManager::Get<RF_Gfx2D>(package, background);
  }
  if(music != "")
  {
    //RF_SoundManager::changeMusic(package, music);
  }

  RF_TextManager::Font = RF_AssetManager::Get<RF_Font>(gui_package, gui_font);

  cursor = RF_Engine::getTask<MainMenu_Cursor>(RF_Engine::newTask<MainMenu_Cursor>(id));
  cursor->SetGfx(package, "cursor");

  errores |= loadMenu(0);

  if(errores)
  {
    RF_Engine::Debug("Error al parsear json");
    exit(-1);
  }
}

void MainMenu::Update()
{
  if(RF_Input::key[_s])
  {
    if(!keyPressed)
    {
      keyPressed = true;
      opc++;
      if(opc >= options.size()){opc = 0;}
    }
  }
  else if(RF_Input::key[_w])
  {
    if(!keyPressed)
    {
      keyPressed = true;
      opc--;
      if(opc < 0){opc = options.size()-1;}
    }
  }
  else if(RF_Input::key[_return])
  {
    if(!keyPressed)
    {
      keyPressed = true;
      optionSelected();
    }
  }
  else
  {
    keyPressed = false;
  }

  if(cursor->GetPosition() != RF_Engine::getTask(options[opc])->transform.position)
  {
    Vector2<float> tmpPos = RF_Engine::getTask(options[opc])->transform.position;
    tmpPos.y += 12;
    tmpPos.x += RF_Engine::getTask(options[opc])->graph->w*0.5 + cursor->graph->w*0.5;
    cursor->SetPosition(tmpPos);
  }
}

void MainMenu::optionSelected()
{
  switch(depth)
  {
    case 0:
    {
      if(values[opc] == "start")
      {
        MainProcess::instance->ChangeScene(2);
      }
      else if(values[opc] == "options")
      {
        loadMenu(1);
      }
      else
      {
        RF_Engine::Status() = false;
      }
    }
    break;

    case 1:
    {
      if(values[opc] == "menu_back")
      {
        loadMenu(0);
      }
      else if(values[opc] == "lang_menu")
      {
        loadMenu(2);
      }
    }
    break;

    case 2:
    {
      if(values[opc] != "menu_back")
      {
        Locale::setLang(values[opc]);
        Globals::setConfig("lang", values[opc]);
      }
      loadMenu(1);
    }
    break;
  }
}
