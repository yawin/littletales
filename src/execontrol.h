#ifndef EXECONTROL_H
#define EXECONTROL_H

#include "mainprocess.h"

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_structs.h>
#include <RosquilleraReforged/rf_input.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_taskmanager.h>

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

class ExeControl : public RF_Process
{
  public:
    ExeControl():RF_Process("ExeControl"){}
    virtual ~ExeControl(){}

    virtual void Update()
    {
      if(RF_Input::key[_esc] || RF_Input::jkey[_home])
      {
        if(!escPressed)
        {
          escPressed = true;
          if(MainProcess::instance->actualScene > 1)
          {
            MainProcess::instance->ChangeScene(_MAIN_MENU);
          }
          else
          {
            RF_Engine::Status() = false;
          }
        }
      }
      else
      {
        escPressed = false;
      }

      if(RF_Input::key[_close_window])
      {
        RF_Engine::Status() = false;
      }

      /*if(((RF_Input::key[_return] && !RF_Input::key[_tab] && RF_Engine::MainWindow()->hasFocus()) || RF_Input::jkey[_start]) && MainProcess::instance->actualScene == 1)
      {
        MainProcess::instance->ChangeScene(_GAME);
      }*/
    }

    bool escPressed = false;
};

#endif //EXECONTROL_H
